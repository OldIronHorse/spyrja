# Spyrja  [![pipeline status](https://gitlab.com/OldIronHorse/spyrja/badges/master/pipeline.svg)](https://gitlab.com/OldIronHorse/spyrja/-/commits/master)  [![coverage report](https://gitlab.com/OldIronHorse/spyrja/badges/master/coverage.svg)](https://gitlab.com/OldIronHorse/spyrja/-/commits/master) 

(spear-ee-ah)

Query functions for collections of named tuples.

## Installation

`pip install spyrja`

## Usage

See examples/*.py for happy path examples.

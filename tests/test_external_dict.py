import pytest
from collections import namedtuple

from spyrja import get, update, remove


Person = namedtuple('Person', 'id first_name last_name location')
Room = namedtuple('Room', 'id name location capacity')
Animal = namedtuple('Animal', 'name species number_of_legs')


def test_get_empty_all():
    assert {} == dict(get({}))


def test_get_empty_by_id():
    assert {} == dict(get({}, id='1234'))


def test_get_empty_by_fields():
    assert {} == dict(get({}, first_name='John', last_name='Lennon'))


def test_get_empty_by_function():
    assert {} == dict(get({}, capacity=lambda c: c > 15))


@pytest.fixture()
def populated_dict():
    return {
            '1234': Person('1234', 'John', 'Lennon', 'Liverpool'),
            '1235': Person('1235', 'Ringo', 'Star', 'London'),
            '1236': Room('1236', 'Asia', 'London', 20),
            '1237': Room('1237', 'Africa', 'Manchester', 10),
            }


def test_get_populated_all(populated_dict):
    assert populated_dict == dict(get(populated_dict))


def test_get_populated_by_id(populated_dict):
    assert {
            '1234': Person('1234', 'John', 'Lennon', 'Liverpool'),
            } == dict(get(populated_dict, id='1234'))


def test_get_populated_by_id_miss(populated_dict):
    assert {} == dict(get(populated_dict, id='1233'))


def test_get_populated_by_fields(populated_dict):
    assert {
            '1234': Person('1234', 'John', 'Lennon', 'Liverpool'),
            } == dict(get(populated_dict,
                          first_name='John',
                          last_name='Lennon'))


def test_get_populated_by_fields_heterogeneous(populated_dict):
    assert {
            '1235': Person('1235', 'Ringo', 'Star', 'London'),
            '1236': Room('1236', 'Asia', 'London', 20),
            } == dict(get(populated_dict, location='London'))


def test_get_populated_by_fields_miss(populated_dict):
    assert {} == dict(get(populated_dict,
                          first_name='Julian',
                          last_name='Lennon'))


def test_get_populated_by_function(populated_dict):
    assert {
            '1236': Room('1236', 'Asia', 'London', 20),
            } == dict(get(populated_dict, capacity=lambda c: c > 15))


def test_get_populated_by_function_miss(populated_dict):
    assert {} == dict(get(populated_dict, capacity=lambda c: c > 150))


def test_update_by_field(populated_dict):
    updated_dict = dict(update(populated_dict,
                               {'location': 'Paris'},
                               location='London'))
    assert {
            '1234': Person('1234', 'John', 'Lennon', 'Liverpool'),
            '1235': Person('1235', 'Ringo', 'Star', 'Paris'),
            '1236': Room('1236', 'Asia', 'Paris', 20),
            '1237': Room('1237', 'Africa', 'Manchester', 10),
            } == updated_dict
    assert populated_dict != updated_dict


def test_remove_all(populated_dict):
    assert {} == dict(remove(populated_dict))


def test_remove_by_field(populated_dict):
    assert {
            '1234': Person('1234', 'John', 'Lennon', 'Liverpool'),
            '1235': Person('1235', 'Ringo', 'Star', 'London'),
            '1237': Room('1237', 'Africa', 'Manchester', 10),
            } == dict(remove(populated_dict, capacity=20))

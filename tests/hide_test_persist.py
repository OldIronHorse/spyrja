from unittest import TestCase
from collections import namedtuple
import os

from spyrja.persist import register, get_db, store, fetch


Person = namedtuple('Person', 'id first_name last_name location')
Room = namedtuple('Room', 'id name location capacity')
Animal = namedtuple('Animal', 'name species number_of_legs')

register(Person)
register(Room)
register(Animal)


class WithCleanTestDB(TestCase):
    def setUp(self):
        self.db = get_db(
                host=os.environ.get('MONGODB_HOST', 'localhost'),
                port=int(os.environ.get('MONGODB_PORT', '27017')),
                db_name='test_spyrja')
        for collection_name in self.db.list_collection_names():
            self.db[collection_name].drop()

    def tearDown(self):
        for collection_name in self.db.list_collection_names():
            self.db[collection_name].drop()


class TestFlatList(WithCleanTestDB):
    def test_empty(self):
        store(self.db, 'people', [])
        self.assertEqual([], fetch(self.db, 'people'))

    def test_single_item(self):
        store(self.db, 'people', [Person(1234, 'Bob', 'Geldorf', 'Ireland')])
        self.assertEqual(
                [Person(1234, 'Bob', 'Geldorf', 'Ireland')],
                fetch(self.db, 'people'))

    def test_multiple_items(self):
        store(self.db, 'people', [
            Person(1235, 'Mick', 'Jagger', 'England'),
            Person(1233, 'Roddy', 'Woomble', 'Scotland'),
            Person(1234, 'Bob', 'Geldorf', 'Ireland')])
        self.assertEqual([
            Person(1235, 'Mick', 'Jagger', 'England'),
            Person(1233, 'Roddy', 'Woomble', 'Scotland'),
            Person(1234, 'Bob', 'Geldorf', 'Ireland')],
            fetch(self.db, 'people'))


class TestFlatSet(WithCleanTestDB):
    def test_empty(self):
        store(self.db, 'people', set())
        self.assertEqual([], fetch(self.db, 'people'))
        #  If the collection is empty the type doesn't matter

    def test_single_item(self):
        store(self.db, 'people', {Person(1234, 'Bob', 'Geldorf', 'Ireland')})
        self.assertEqual(
                {Person(1234, 'Bob', 'Geldorf', 'Ireland')},
                fetch(self.db, 'people'))

    def test_multiple_items(self):
        store(self.db, 'people', {
            Person(1235, 'Mick', 'Jagger', 'England'),
            Person(1233, 'Roddy', 'Woomble', 'Scotland'),
            Person(1234, 'Bob', 'Geldorf', 'Ireland')})
        self.assertEqual({
            Person(1235, 'Mick', 'Jagger', 'England'),
            Person(1233, 'Roddy', 'Woomble', 'Scotland'),
            Person(1234, 'Bob', 'Geldorf', 'Ireland')},
            fetch(self.db, 'people'))


class TestFlatDict(WithCleanTestDB):
    def test_empty(self):
        store(self.db, 'animals', dict())
        self.assertEqual([], fetch(self.db, 'animals'))
        #  If the collection is empty the type doesn't matter

    def test_single_item(self):
        store(self.db, 'animals', {'Fido': Animal('Fido', 'dog', 4)})
        self.assertEqual(
                {'Fido': Animal('Fido', 'dog', 4)},
                fetch(self.db, 'animals'))

# TODO: dict storage
# TODO: nested collections
# TODO: referential loops (detection?)

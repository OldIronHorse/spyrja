import pytest
from collections import namedtuple
import shelve

from spyrja import get


Person = namedtuple('Person', 'id first_name last_name location')
Room = namedtuple('Room', 'id name location capacity')
Animal = namedtuple('Animal', 'name species number_of_legs')


def test_get_empty_all(tmp_path):
    with shelve.open(tmp_path / 'store.db') as store:
        assert {} == dict(get(store))


def test_get_empty_by_id(tmp_path):
    with shelve.open(tmp_path / 'store.db') as store:
        assert {} == dict(get(store, id='1234'))


def test_get_empty_by_fields(tmp_path):
    with shelve.open(tmp_path / 'store.db') as store:
        assert {} == dict(get(store, first_name='John', last_name='Lennon'))


def test_get_empty_by_function(tmp_path):
    with shelve.open(tmp_path / 'store.db') as store:
        assert {} == dict(get(store, capacity=lambda c: c > 15))


PEOPLE_AND_ROOMS = {
    '1234': Person('1234', 'John', 'Lennon', 'Liverpool'),
    '1235': Person('1235', 'Ringo', 'Star', 'London'),
    '1236': Room('1236', 'Asia', 'London', 20),
    '1237': Room('1237', 'Africa', 'Manchester', 10),
    }


@pytest.fixture()
def populated_dict_path(tmp_path):
    path = tmp_path / 'store.self'
    with shelve.open(path) as store:
        for k, v in PEOPLE_AND_ROOMS.items():
            store[k] = v
    return path


def test_get_populated_all(populated_dict_path):
    with shelve.open(populated_dict_path) as store:
        assert PEOPLE_AND_ROOMS == dict(get(store))


def test_get_populated_by_id(populated_dict_path):
    with shelve.open(populated_dict_path) as store:
        assert {
                '1234': Person('1234', 'John', 'Lennon', 'Liverpool'),
                } == dict(get(store, id='1234'))


def test_get_populated_by_id_miss(populated_dict_path):
    with shelve.open(populated_dict_path) as store:
        assert {} == dict(get(store, id='1233'))


def test_get_populated_by_fields(populated_dict_path):
    with shelve.open(populated_dict_path) as store:
        assert {
                '1234': Person('1234', 'John', 'Lennon', 'Liverpool'),
                } == dict(get(store,
                              first_name='John',
                              last_name='Lennon'))


def test_get_populated_by_fields_heterogeneous(populated_dict_path):
    with shelve.open(populated_dict_path) as store:
        assert {
                '1235': Person('1235', 'Ringo', 'Star', 'London'),
                '1236': Room('1236', 'Asia', 'London', 20),
                } == dict(get(store, location='London'))


def test_get_populated_by_fields_miss(populated_dict_path):
    with shelve.open(populated_dict_path) as store:
        assert {} == dict(get(store,
                              first_name='Julian',
                              last_name='Lennon'))


def test_get_populated_by_function(populated_dict_path):
    with shelve.open(populated_dict_path) as store:
        assert {
                '1236': Room('1236', 'Asia', 'London', 20),
                } == dict(get(store, capacity=lambda c: c > 15))


def test_get_populated_by_function_miss(populated_dict_path):
    with shelve.open(populated_dict_path) as store:
        assert {} == dict(get(store, capacity=lambda c: c > 150))


def test_update_by_field(populated_dict_path):
    with shelve.open(populated_dict_path) as store:
        for k, v in get(store, location='London'):
            store[k] = v._replace(location='Paris')
    with shelve.open(populated_dict_path) as store:
        assert {
                '1234': Person('1234', 'John', 'Lennon', 'Liverpool'),
                '1235': Person('1235', 'Ringo', 'Star', 'Paris'),
                '1236': Room('1236', 'Asia', 'Paris', 20),
                '1237': Room('1237', 'Africa', 'Manchester', 10),
                } == dict(get(store))


def test_remove_all(populated_dict_path):
    with shelve.open(populated_dict_path) as store:
        for k, v in get(store):
            del store[k]
    with shelve.open(populated_dict_path) as store:
        assert {} == dict(get(store))


def test_remove_by_field(populated_dict_path):
    with shelve.open(populated_dict_path) as store:
        for k, v in get(store, capacity=20):
            del store[k]
    with shelve.open(populated_dict_path) as store:
        assert {
                '1234': Person('1234', 'John', 'Lennon', 'Liverpool'),
                '1235': Person('1235', 'Ringo', 'Star', 'London'),
                '1237': Room('1237', 'Africa', 'Manchester', 10),
                } == dict(get(store))

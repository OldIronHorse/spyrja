import pytest
from collections import namedtuple

from spyrja import get, update, remove


Person = namedtuple('Person', 'id first_name last_name location')
Room = namedtuple('Room', 'id name location capacity')
Animal = namedtuple('Animal', 'name species number_of_legs')


def test_get_all():
    assert set() == set(get(set()))


def test_get_by_id():
    assert set() == set(get(set(), id='1234'))


def test_get_by_fields():
    assert set() == set(get(set(), first_name='John', last_name='Lennon'))


def test_get_by_fn():
    assert set() == set(get(set(), capacity=lambda c: c > 15))


@pytest.fixture()
def populated_set():
    return {
            Person('1234', 'John', 'Lennon', 'Liverpool'),
            Person('1235', 'Ringo', 'Star', 'London'),
            Room('1236', 'Asia', 'London', 20),
            Room('1237', 'Africa', 'Manchester', 10),
            }


def test_get_populated_all(populated_set):
    assert populated_set == set(get(populated_set))


def test_get_populated_by_id(populated_set):
    assert {
            Person('1234', 'John', 'Lennon', 'Liverpool'),
            } == set(get(populated_set, id='1234'))


def test_get_populated_by_id_miss(populated_set):
    assert set() == set(get(populated_set, id='1233'))


def test_get_populated_by_fields(populated_set):
    assert {
            Person('1234', 'John', 'Lennon', 'Liverpool'),
            } == set(get(populated_set, first_name='John', last_name='Lennon'))


def test_get_populated_by_fields_heterogeneous(populated_set):
    assert {
            Person('1235', 'Ringo', 'Star', 'London'),
            Room('1236', 'Asia', 'London', 20),
            } == set(get(populated_set, location='London'))


def test_get_populated_by_fields_miss(populated_set):
    assert set() == set(get(populated_set,
                            first_name='Julian',
                            last_name='Lennon'))


def test_get_populated_by_fn(populated_set):
    assert {
            Room('1236', 'Asia', 'London', 20),
            } == set(get(populated_set, capacity=lambda c: c > 15))


def test_get_populated_by_fn_miss(populated_set):
    assert set() == set(get(populated_set, capacity=lambda c: c > 150))


def test_update_by_field(populated_set):
    updated_set = set(update(populated_set,
                             dict(location='Paris'),
                             location='London'))
    assert {
            Person('1234', 'John', 'Lennon', 'Liverpool'),
            Person('1235', 'Ringo', 'Star', 'Paris'),
            Room('1236', 'Asia', 'Paris', 20),
            Room('1237', 'Africa', 'Manchester', 10),
            } == updated_set
    assert populated_set != updated_set


def test_update_all(populated_set):
    updated_set = set(update(populated_set, dict(location='Paris')))
    assert {
            Person('1234', 'John', 'Lennon', 'Paris'),
            Person('1235', 'Ringo', 'Star', 'Paris'),
            Room('1236', 'Asia', 'Paris', 20),
            Room('1237', 'Africa', 'Paris', 10),
            } == updated_set
    assert populated_set != updated_set


def test_remove_all(populated_set):
    assert set() == set(remove(populated_set))


def test_remove_by_id(populated_set):
    assert {
            Person('1234', 'John', 'Lennon', 'Liverpool'),
            Person('1235', 'Ringo', 'Star', 'London'),
            Room('1237', 'Africa', 'Manchester', 10),
            } == set(remove(populated_set, id='1236'))

import pytest
from collections import namedtuple

from spyrja import get, update, remove


Person = namedtuple('Person', 'id first_name last_name location')
Room = namedtuple('Room', 'id name location capacity')
Animal = namedtuple('Animal', 'name species number_of_legs')


def test_get_all_empty():
    assert [] == list(get([]))


def test_get_by_id_empty():
    assert [] == list(get([], id='1234'))


def test_get_by_fields_empty():
    assert [] == list(get([], first_name='John', last_name='Lennon'))


def test_get_by_fn_empty():
    assert [] == list(get([], capacity=lambda c: c > 15))


@pytest.fixture()
def populated_list():
    return [
            Person('1234', 'John', 'Lennon', 'Liverpool'),
            Person('1235', 'Ringo', 'Star', 'London'),
            Room('1236', 'Asia', 'London', 20),
            Room('1237', 'Africa', 'Manchester', 10),
            ]


def test_get_all(populated_list):
    assert [
            Person('1234', 'John', 'Lennon', 'Liverpool'),
            Person('1235', 'Ringo', 'Star', 'London'),
            Room('1236', 'Asia', 'London', 20),
            Room('1237', 'Africa', 'Manchester', 10),
            ] == list(get(populated_list))


def test_get_by_id(populated_list):
    assert [
            Person('1234', 'John', 'Lennon', 'Liverpool'),
            ] == list(get(populated_list, id='1234'))


def test_get_by_id_miss(populated_list):
    assert list() == list(get(populated_list, id='1233'))


def test_get_by_fields(populated_list):
    assert [
            Person('1234', 'John', 'Lennon', 'Liverpool'),
            ] == list(get(populated_list,
                          first_name='John',
                          last_name='Lennon'))


def test_get_by_fields_heterogeneous(populated_list):
    assert [
            Person('1235', 'Ringo', 'Star', 'London'),
            Room('1236', 'Asia', 'London', 20),
            ] == list(get(populated_list, location='London'))


def test_get_by_fields_miss(populated_list):
    assert list() == list(get(populated_list,
                              first_name='Julian',
                              last_name='Lennon'))


def test_get_by_fn(populated_list):
    assert [
            Room('1236', 'Asia', 'London', 20),
            ] == list(get(populated_list,
                          capacity=lambda c: c > 15))


def test_get_by_fn_miss(populated_list):
    assert list() == list(get(populated_list,
                              capacity=lambda c: c > 150))


def test_update_by_field(populated_list):
    assert [
            Person('1234', 'John', 'Lennon', 'Liverpool'),
            Person('1235', 'Ringo', 'Star', 'Paris'),
            Room('1236', 'Asia', 'Paris', 20),
            Room('1237', 'Africa', 'Manchester', 10),
            ] == list(update(populated_list,
                             dict(location='Paris'),
                             location='London'))
    assert [
            Person('1234', 'John', 'Lennon', 'Liverpool'),
            Person('1235', 'Ringo', 'Star', 'London'),
            Room('1236', 'Asia', 'London', 20),
            Room('1237', 'Africa', 'Manchester', 10),
            ] == populated_list


def test_update_all(populated_list):
    assert [
            Person('1234', 'John', 'Lennon', 'Paris'),
            Person('1235', 'Ringo', 'Star', 'Paris'),
            Room('1236', 'Asia', 'Paris', 20),
            Room('1237', 'Africa', 'Paris', 10),
            ] == list(update(populated_list,
                             dict(location='Paris')))
    assert [
            Person('1234', 'John', 'Lennon', 'Liverpool'),
            Person('1235', 'Ringo', 'Star', 'London'),
            Room('1236', 'Asia', 'London', 20),
            Room('1237', 'Africa', 'Manchester', 10),
            ] == populated_list


def test_remove_all(populated_list):
    assert [] == list(remove(populated_list))


def test_remove_by_id(populated_list):
    assert [
            Person('1234', 'John', 'Lennon', 'Liverpool'),
            Person('1235', 'Ringo', 'Star', 'London'),
            Room('1237', 'Africa', 'Manchester', 10),
            ] == list(remove(populated_list, id='1236'))

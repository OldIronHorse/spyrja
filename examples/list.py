from collections import namedtuple
from pprint import PrettyPrinter

from spyrja import get, update, remove

pp = PrettyPrinter()

Person = namedtuple('Person', 'name age')
Room = namedtuple('Room', 'name location capacity')

people = [
        Person('John', 53),
        Person('Jane', 43),
        Person('Dave', 33),
        Room('Big Meeting Room', 'London', 20),
        Room('Small Meeting Room', 'London', 10),
        Room('Lecture Theatre', 'Manchester', 100)]

print('get all:')
pp.pprint(list(get(people)))

print("get name='Jane':")
pp.pprint(list(get(people, name='Jane')))

print('get age > 40:')
pp.pprint(list(get(people, age=lambda a: a > 40)))

print("get location='London' and capacity > 15:")
pp.pprint(list(get(people, location='London', capacity=lambda c: c > 15)))

print("get name is 'Dave' or 'Big Meeting Room':")
pp.pprint(list(get(people, name=lambda n: n in ['Dave', 'Big Meeting Room'])))

print("move larger meeting rooms from London to Paris:")
pp.pprint(list(update(
        people,
        dict(location='Paris'),
        location='London',
        capacity=lambda c: c > 15)))

print("demolish the small meeting rooms in London:")
pp.pprint(list(remove(
        people,
        location='London',
        capacity=lambda c: c < 15)))

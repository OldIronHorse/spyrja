from collections import namedtuple
from pprint import PrettyPrinter

from spyrja import get, update, remove

pp = PrettyPrinter()

Person = namedtuple('Person', 'name age')
Room = namedtuple('Room', 'name location capacity')

people = {
        1234: Person('John', 53),
        1235: Person('Jane', 43),
        1236: Person('Dave', 33),
        'r001': Room('Big Meeting Room', 'London', 20),
        'r002': Room('Small Meeting Room', 'London', 10),
        'r003': Room('Lecture Theatre', 'Manchester', 100)}

print('get all:')
pp.pprint(dict(get(people)))

print("get name='Jane':")
pp.pprint(dict(get(people, name='Jane')))

print('get age > 40:')
pp.pprint(dict(get(people, age=lambda a: a > 40)))

print("get location='London' and capacity > 15:")
pp.pprint(dict(get(people, location='London', capacity=lambda c: c > 15)))

print("get name is 'Dave' or 'Big Meeting Room':")
pp.pprint(dict(get(people, name=lambda n: n in ['Dave', 'Big Meeting Room'])))

print("move larger meeting rooms from London to Paris:")
pp.pprint(dict(update(
        people,
        dict(location='Paris'),
        location='London',
        capacity=lambda c: c > 15)))

print("demolish the small meeting rooms in London:")
pp.pprint(dict(remove(
        people,
        location='London',
        capacity=lambda c: c < 15)))

print("move meeting rooms from London to Paris (in place):")
for k, _ in get(people, location='London', capacity=lambda c: True):
    people[k] = people[k]._replace(location='Paris')
pp.pprint(people)

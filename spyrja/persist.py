from pymongo import MongoClient
from pymongo.errors import InvalidOperation
import re

connections = dict()
tuples_by_name = dict()


def register(nt):
    tuples_by_name[nt.__name__] = nt


def get_db(db_name, host='localhost', port=27017):
    global connections
    try:
        return connections[f'{host}:{port}'][db_name]
    except KeyError:
        connections[f'{host}:{port}'] = MongoClient(host, port)
        return connections[f'{host}:{port}'][db_name]


def as_named_dict(named_tuple):
    d = named_tuple._asdict()
    d['__name__'] = named_tuple.__class__.__name__
    d['_id'] = d['id']
    del d['id']
    return d


def store(db, name, coll):
    collection_name = f'{name}__{coll.__class__.__name__}'
    try:
        print('inserting:', collection_name, coll)
        to_insert = []
        if coll.__class__.__name__ == 'dict':
            for k, v in coll.items():
                d = v._asdict()
                d['__name__'] = v.__class__.__name__
                d['_id'] = k
                to_insert.append(d)
            db[collection_name].insert_many(to_insert)
        else:
            db[collection_name].insert_many((as_named_dict(i) for i in coll))
    except TypeError as e:
        if e.args == ('documents must be a non-empty list',):
            # empty collection
            pass
        else:
            raise e
    except InvalidOperation as e:
        if e.args == ('No operations to execute',):
            # empty collection
            pass
        else:
            raise e


def from_named_dict(d):
    d['id'] = d['_id']
    del d['_id']
    return tuples_by_name[d['__name__']](**{k: v for (k, v) in d.items()
                                         if k != '__name__'})


def dict_from_named_dict(db_coll):
    d = dict()
    for i in db_coll.find({}):
        print(i)
        d[i['_id']] = tuples_by_name[i['__name__']](
                **{k: v for (k, v) in i.items()
                    if k != '_id' and k != '__name__'})
    return d


collection_builders = {
        'dict': dict_from_named_dict,
        'list': lambda db_coll: [from_named_dict(i) for i in db_coll.find({})],
        'set': lambda db_coll: {from_named_dict(i) for i in db_coll.find({})}}


def fetch(db, name):
    print(db.list_collection_names())
    p = re.compile(f'{name}__(list|set|dict)')
    for coll_name in db.list_collection_names():
        print(coll_name)
        m = p.match(coll_name)
        if m:
            print(coll_name, m.group(1))
            print(list(db[coll_name].find({})))
            return collection_builders[m.group(1)](db[coll_name])
    return []
